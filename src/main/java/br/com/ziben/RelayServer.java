package br.com.ziben;

import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

/**
 * class realy sockets threading
 * @author Claudio Cardozo
 * 
 */
class RelayServer extends Thread {
	private static final Logger LOG = Logger.getLogger(RelayServer.class.getName());

	int socketPort;
	int maxThreads;
	int threadCount;

	public RelayServer(int p, int m) {
		socketPort = p; // internal port to support clients
		maxThreads = m; // maximum threads number
		threadCount = 0; // just a counter

		run();
	}

	public void run() {
		ServerSocket serverSocket = null;
		Socket acceptSocket = null;
		try {
			serverSocket = new ServerSocket(socketPort);
			while (true) {
				System.out.println("Accepting port: " + socketPort + " MaxThreads: " + maxThreads);
				LOG.debug("Accepting port: " + socketPort + " MaxThreads: " + maxThreads);
				serverSocket.setReceiveBufferSize(4096);
				acceptSocket = serverSocket.accept();
				// System.out.println("Relaying...");
				if (threadCount < maxThreads) {
					threadCount++;
					System.out.println("creating thread: " + threadCount);
					LOG.debug("creating thread: " + threadCount);
					new RelayThread(acceptSocket, this);
				}
			}
		} catch (Exception e) {
			System.out.println("RelayServer Error");
		} finally {
			System.out.println("ending thread: " + threadCount);
			LOG.debug("ending thread: " + threadCount);
		}
/*		try {
			serverSocket.close();
			acceptSocket.close();
		} catch (IOException e) {
			System.out.println("ERROR Fechando socket: " + threadCount);
			e.printStackTrace();
		}*/
	}

	/**
	 * tell that socket is closed, than, decrement the threads counter 
	 */
	void tellclosed() {
		//System.out.println("Closing thread...");
		//LOG.debug("Closing thread...");
		threadCount--;
		// TODO threading controller
		if(threadCount < 0) {
			threadCount = 0;
		}
	}
}