/**
 * @author Claudio Cardozo Tantan
 */
package br.com.ziben;

import org.apache.log4j.Logger;

/**
 * class for single TCP relay packets
 * @author ccardozo
 *
 */
class Relay {
	private static final Logger LOG = Logger.getLogger(RelayServer.class.getName());
	// sorry by the static... it comes from my C++!
	static ResourceBundleOwn rb = new ResourceBundleOwn();

	// entry point
	public static void main(String args[]) {
		int port;
		int maxThreads;
		String propertiesbundle = "";

		// getting the properties file name
		try {
			propertiesbundle = args[2];
		} catch (Exception e) {
			System.err.println("Properties filename argument must be a String");
			System.err.println("RelayTantan 9000 50 ./relay.properties");
			System.exit(1);
		}
		// you need to inform the arguments on command line
		try {
			port = Integer.parseInt(args[0]);
			maxThreads = Integer.parseInt(args[1]);
		} catch (Exception e) {
			// defensive programming... sometimes, come in some bad guys! argh!
			port = 4000;
			maxThreads = 100;
			System.out.println("Relaying by default Port=" + port);
			System.out.println("Relaying by default maxThreads=" + maxThreads);
		}
		// setting the static properties using a class to be acessed by threads
		rb.setProperties(propertiesbundle);
		// rb.getProperties();
		System.out.println("Port=" + port + " max=" + maxThreads + " prop="
				+ propertiesbundle + " bundle=" + rb.getProperties());
		LOG.debug("Port=" + port + " max=" + maxThreads + " prop="
				+ propertiesbundle + " bundle=" + rb.getProperties());

		// start relay server multithreaded
		new RelayServer(port, maxThreads);

	}
}
