package br.com.ziben;

/**
 * class for properties file name handle
 * @author Claudio Cardozo
 * 
 */
class ResourceBundleOwn {
	String propertyName;

	public void setProperties(String prop) {
		propertyName = prop;
	}

	public String getProperties() {
		return propertyName;
	}
}