package br.com.ziben;

import java.io.DataInputStream;
import java.io.PrintStream;

import org.apache.log4j.Logger;

/**
 * class for threads sockets control
 * @author ccardozo
 *
 */
class RelayInputThread extends Thread {
	private static final Logger LOG = Logger.getLogger(RelayServer.class.getName());

	DataInputStream inStream;
	PrintStream outStream;
	RelayThread relayThread;

	RelayInputThread(DataInputStream in, PrintStream out, RelayThread rt) {
		inStream = in;
		outStream = out;
		relayThread = rt;
		start();
	}

	public void run() {
		try {
			LOG.debug("just relaying packets... go ahead");;
			while (true) // iuupiii!!! just relaying packets... go ahead
			{
				byte readBytes = inStream.readByte(); // take it from here
				outStream.write(readBytes); // put here...
			}
		} catch (Exception e) {
		}
		LOG.debug("relayThread.closeall()... finished");;
		relayThread.closeall();
	}
}