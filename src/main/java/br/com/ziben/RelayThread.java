package br.com.ziben;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Properties;

import org.apache.log4j.Logger;


/**
 * Class for threading control
 * @author ccardozo
 *
 */
class RelayThread extends Thread {
	private static final Logger LOG = Logger.getLogger(RelayThread.class.getName());

	Socket inputSocket, destinationSocket;
	DataInputStream inputStream, destinationInput;
	PrintStream outputStream, destinationOutput;
	RelayServer relayServer;

	boolean socketClosed = false;

	RelayThread(Socket s, RelayServer rs) {
		inputSocket = s; // socket open
		relayServer = rs; // relay server
		start();
	}

	/**
	 * run method from interface
	 */
	public void run() {

		try {
			// get the resource name
			String resourceName = Relay.rb.getProperties();
			// a path for the properties file
			File file = new File(resourceName);
			// relay properties
			Properties props = new Properties();
			FileInputStream fis = null;

			try {
				// read the properties file
				fis = new FileInputStream(file);
				props.load(fis);
				fis.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}

			// System.out.println("rodando o thread
			// antes...getBundle="+resourceName);
			// System.out.println("path: "+ relayPath);
			// res = ResourceBundle.getBundle(relayPath+resourceName);
			// System.out.println("rodando o thread apos getBundle...");
			// System.out.println("IP : " + res.getString("Bundle" + ".IP"));
			// System.out.println("Port : " + res.getString("Bundle" +
			// ".Port"));

			inputStream = new DataInputStream(inputSocket.getInputStream()); // input stream
			// connection
			outputStream = new PrintStream(new DataOutputStream(inputSocket.getOutputStream())); // output
			// stream connection
			String server = props.getProperty("Bundle" + ".IP"); // destination
			// port on destination
			int port = Integer.parseInt(props.getProperty("Bundle" + ".Port")); // port
			// on destination
			destinationSocket = new Socket(server, port);
			// set receive and send buffer size
			destinationSocket.setReceiveBufferSize(4096);
			destinationSocket.setSendBufferSize(4096);
			destinationSocket.setSoTimeout(30000);
			// streaming...
			destinationOutput = new PrintStream(new DataOutputStream(destinationSocket.getOutputStream()));
			destinationInput = new DataInputStream(destinationSocket.getInputStream());
			new RelayInputThread(inputStream, destinationOutput, this);
			System.out.println("Reading / writing bytes...");
			LOG.debug("Reading / writing bytes...");
			while (true) // just relaying
			{
				//System.out.println("Reading bytes...");
				//LOG.debug("Reading bytes...");
				
				//byte b = IgsIn.readByte();
				int b = 0;
				// reading from sockets client
				b = destinationInput.readUnsignedByte();
				//System.out.println("Writing bytes...");
				// writing to the target
				outputStream.write(b);
				
			}
			// it always will get exception, because the closeall come from RelayInputThread
		} catch (Exception e) { 
			//e.printStackTrace();
			//LOG.debug("ERROR: " + e.getStackTrace());
		}
		
		System.out.println("Finalizou a thread...");
		LOG.debug("Finalizou a thread...");
		closeall();
	}

	/**
	 * Close all connections open
	 * 
	 */
	void closeall() {
		//LOG.debug("Closing thread: " + relayServer.threadCount);
		try {
			inputStream.close();
		} catch (Exception e) {
		}
		try {
			outputStream.close();
		} catch (Exception e) {
		}
		try {
			destinationInput.close();
		} catch (Exception e) {
		}
		try {
			destinationOutput.close();
		} catch (Exception e) {
		}
		try {
			destinationSocket.close();
		} catch (Exception e) {
		}
		try {
			inputSocket.close();
		} catch (Exception e) {
		}
		if (!socketClosed) {
			relayServer.tellclosed();
			socketClosed = true;
		}
	}
}